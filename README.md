# sylva-prometheus-rules

Generate `PrometheusRule` objects for consumption by Prometheus

## Overview

There are two mechanisms that control which rules are deployed

1. `createRules` selects which directories are considered
2. `optional_rules` selects which files in those directories are added to the Configmap

## Rules overrides

`.Values.createRules` controls which cluster rules are checked and the keys represent the directories under `alert-rules/`

If `.Values.createRules.allclusters` is `true` (default) then the `alert-rules/allclusters/*yaml` rules are parsed last, regardless of what other
clusters are specified

This allows for rule overriding. Example:

```yaml
createRules:
  allclusters: true
  management-cluster: true
```

```
alert-rules/allclusters/health-alerts.yaml
alert-rules/allclusters/dummy.yaml

alert-rules/management-cluster/flux.yaml
alert-rules/management-cluster/health-alerts.yaml
alert-rules/management-cluster/minio.yaml
```

- First the `PrometheusRule` with the *flux*, *minio* and *health-alerts* name from `management-cluster` are created.
- Then *health-alerts* and *dummy* from `allcluster` are parsed. Since *health-alerts* is already applied from `mananagement-cluster` it will **not** be applied again. *dummy* will be applied since it doesn't override anything

This in effect allows the user to override the *health-alerts* from `allclusters` with *health-alerts* form `management-cluster`

## Rules activation

`.Values.optional_rules` controls which rules are enabled for optional components

