groups:
- name: SNMP DELL iDRAC rules
  rules:
  - record: capacity:instance:power_consumption:sum
    expr: label_replace(sum by (model,alias)(((powerUsageCumulativeWattage - powerUsageCumulativeWattage offset 60m) or (rate(powerUsageCumulativeWattage[10m])*3600)) * on (instance) group_left(model) label_replace(systemModelName ,"model","$1","systemModelName","(.*)")), "alias", "$1", "alias", "([^.]+).*")
    labels:
      category: Hardware
      type: Server
      vendor: Dell

  - alert: SNMP_DELL_iDRAC_globalSystemStatus_NOK
    expr: globalSystemStatus{job="snmp-exporter",module="dell_idrac",globalSystemStatus!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - globalSystemStatus is NOK. Current state is: {{ $labels.globalSystemStatus }}'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC globalSystemStatus NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateBatteryStatusCombined_NOK
    expr: systemStateBatteryStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateBatteryStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateBatteryStatus is NOK. Current state is: {{ $labels.systemStateBatteryStatusCombined }}. Check RAID Controller BBU or CMOS battery in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateBatteryStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateCoolingDeviceStatusCombined_NOK
    expr: systemStateCoolingDeviceStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateCoolingDeviceStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateCoolingDeviceStatus is NOK. Current state is: {{ $labels.systemStateCoolingDeviceStatusCombined }}. Check system fans in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateCoolingDeviceStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateCoolingUnitStatusCombined_NOK
    expr: systemStateCoolingUnitStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateCoolingUnitStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateCoolingDeviceStatus is NOK. Current state is: {{ $labels.systemStateCoolingUnitStatusCombined }}. Check system fans in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateCoolingUnitStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateMemoryDeviceStatusCombined_NOK
    expr: systemStateMemoryDeviceStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateMemoryDeviceStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateMemoryDeviceStatus is NOK. Current state is: {{ $labels.systemStateMemoryDeviceStatusCombined }}. Check system volatile memory in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateMemoryDeviceStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStatePowerSupplyStatusCombined_NOK
    expr: systemStatePowerSupplyStatusCombined{job="snmp-exporter",module="dell_idrac",systemStatePowerSupplyStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStatePowerSupplyStatus is NOK. Current state is: {{ $labels.systemStatePowerSupplyStatusCombined }}. Check system power supply in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStatePowerSupplyStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStatePowerUnitStatusCombined_NOK
    expr: systemStatePowerUnitStatusCombined{job="snmp-exporter",module="dell_idrac",systemStatePowerUnitStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStatePowerUnitStatus is NOK. Current state is: {{ $labels.systemStatePowerUnitStatusCombined }}. Check system power supply or external power delivery in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStatePowerUnitStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateProcessorDeviceStatusCombined_NOK
    expr: systemStateProcessorDeviceStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateProcessorDeviceStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'critical'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateProcessorDeviceStatus is NOK. Current state is: {{ $labels.systemStateProcessorDeviceStatusCombined }}. Check system processor in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateProcessorDeviceStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateTemperatureStatisticsStatusCombined_NOK
    expr: systemStateTemperatureStatisticsStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateTemperatureStatisticsStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateTemperatureStatisticsStatus is NOK. Current state is: {{ $labels.systemStateTemperatureStatisticsStatusCombined }}. Check system temperatures in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateTemperatureStatisticsStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateTemperatureStatusCombined_NOK
    expr: systemStateTemperatureStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateTemperatureStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateTemperatureStatus is NOK. Current state is: {{ $labels.systemStateTemperatureStatusCombined }}. Check system temperatures in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateTemperatureStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateVoltageStatusCombined_NOK
    expr: systemStateVoltageStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateVoltageStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateVoltageStatus is NOK. Current state is: {{ $labels.systemStateVoltageStatusCombined }}. Check system voltage in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateVoltageStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_systemStateAmperageStatusCombined_NOK
    expr: systemStateAmperageStatusCombined{job="snmp-exporter",module="dell_idrac",systemStateAmperageStatusCombined!="ok"} == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - systemStateAmperageStatus is NOK. Current state is: {{ $labels.systemStateAmperageStatusCombined }}. Check system voltage in iDRAC.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC systemStateAmperageStatusCombined NOK'
  
  - alert: SNMP_DELL_iDRAC_controllerRollUpStatus_NOK
    expr: controllerRollUpStatus{job="snmp-exporter",module="dell_idrac",controllerRollUpStatus!="ok"} * on (controllerNumber,instance,job,module) group_left (controllerName) controllerName == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - controllerRollUpStatus is NOK for controllerNumber {{ $labels.controllerNumber }} ( {{ $labels.controllerName }}). Current state is: {{ $labels.controllerRollUpStatus }}.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC controllerRollUpStatus NOK'
  
  - alert: SNMP_DELL_iDRAC_controllerComponentStatus_NOK
    expr: controllerComponentStatus{job="snmp-exporter",module="dell_idrac",controllerComponentStatus!="ok"} * on (controllerNumber,instance,job,module) group_left (controllerName) controllerName == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - controllerComponentStatus is NOK for controllerNumber {{ $labels.controllerNumber }} ( {{ $labels.controllerName }}). Current state is: {{ $labels.controllerComponentStatus }}.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC controllerComponentStatus NOK'
  
  - alert: SNMP_DELL_iDRAC_physicalDiskState_NOK
    expr: physicalDiskState_info{job="snmp-exporter",module="dell_idrac",physicalDiskState!~"online|ready|non-raid|nonraid"} * on (physicalDiskNumber,instance,job,module) group_left (physicalDiskDisplayName) physicalDiskDisplayName == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - physicalDiskState is NOK for physicalDiskNumber {{ $labels.physicalDiskNumber }} ( {{ $labels.physicalDiskDisplayName }}). Current state is: {{ $labels.physicalDiskState }}.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC physicalDiskState NOK'
  
  - alert: SNMP_DELL_iDRAC_physicalDiskComponentStatus_NOK
    expr: physicalDiskComponentStatus_info{job="snmp-exporter",module="dell_idrac",physicalDiskComponentStatus!="ok"} * on (physicalDiskNumber,instance,job,module) group_left (physicalDiskDisplayName) physicalDiskName == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - physicalDiskComponentStatus is NOK for physicalDiskNumber {{ $labels.physicalDiskNumber }} ( {{ $labels.physicalDiskDisplayName }}). Current state is: {{ $labels.physicalDiskComponentStatus }}.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC physicalDiskComponentStatus NOK'
  
  - alert: SNMP_DELL_iDRAC_physicalDiskSmartAlertIndication_NOK
    expr: physicalDiskSmartAlertIndication{job="snmp-exporter",module="dell_idrac"} * on (physicalDiskNumber,instance,job,module) group_left (physicalDiskDisplayName) physicalDiskDisplayName == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - physicalDiskSmartAlertIndication is NOK for physicalDiskNumber {{ $labels.physicalDiskNumber }} ( {{ $labels.physicalDiskDisplayName }}).'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC physicalDiskSmartAlertIndication NOK'
  
  - alert: SNMP_DELL_iDRAC_physicalDiskRemainingRatedWriteEndurance_WARNING
    expr: (physicalDiskRemainingRatedWriteEndurance{job="snmp-exporter",module="dell_idrac"} * on (physicalDiskNumber,instance,job,module) group_left (physicalDiskDisplayName) physicalDiskDisplayName and on (physicalDiskNumber,instance,job,module) (physicalDiskMediaType_info{physicalDiskMediaType="ssd"} == 1)) < 40
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - physicalDiskRemainingRatedWriteEndurance is less than 40 for physicalDiskNumber {{ $labels.physicalDiskNumber }} ( {{ $labels.physicalDiskDisplayName }}). Value: {{ humanize $value }}'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC physicalDiskRemainingRatedWriteEndurance'
  
  - alert: SNMP_DELL_iDRAC_physicalDiskRemainingRatedWriteEndurance_CRITICAL
    expr: (physicalDiskRemainingRatedWriteEndurance{job="snmp-exporter",module="dell_idrac"} * on (physicalDiskNumber,instance,job,module) group_left (physicalDiskDisplayName) physicalDiskDisplayName and on (physicalDiskNumber,instance,job,module) (physicalDiskMediaType_info{physicalDiskMediaType="ssd"} == 1)) < 20
    for: 5m
    labels:
      severity: 'critical'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - physicalDiskRemainingRatedWriteEndurance is less than 20 for physicalDiskNumber {{ $labels.physicalDiskNumber }} ( {{ $labels.physicalDiskDisplayName }}). Value: {{ humanize $value }}'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC physicalDiskRemainingRatedWriteEndurance'
  
  - alert: SNMP_DELL_iDRAC_virtualDiskState_NOK
    expr: virtualDiskState{job="snmp-exporter",module="dell_idrac",virtualDiskState!="online"} * on (virtualDiskNumber,instance,job,module) group_left (virtualDiskDisplayName) virtualDiskDisplayName == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - virtualDiskState is NOK for virtualDiskNumber {{ $labels.virtualDiskNumber }} ( {{ $labels.virtualDiskDisplayName }}). Current state is: {{ $labels.virtualDiskState }}.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC virtualDiskState NOK'
  
  - alert: SNMP_DELL_iDRAC_virtualDiskComponentStatus_NOK
    expr: virtualDiskComponentStatus{job="snmp-exporter",module="dell_idrac",virtualDiskComponentStatus!="ok"} * on (virtualDiskNumber,instance,job,module) group_left (virtualDiskDisplayName) virtualDiskDisplayName == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - virtualDiskComponentStatus is NOK for virtualDiskNumber {{ $labels.virtualDiskNumber }} ( {{ $labels.virtualDiskDisplayName }}). Current state is: {{ $labels.virtualDiskComponentStatus }}.'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC virtualDiskComponentStatus NOK'
  
  - alert: SNMP_DELL_iDRAC_virtualDiskBadBlocksDetected
    expr: virtualDiskBadBlocksDetected{job="snmp-exporter",module="dell_idrac"} * on (virtualDiskNumber,instance,job,module) group_left (virtualDiskDisplayName) virtualDiskDisplayName == 1
    for: 5m
    labels:
      severity: 'warning'
      sylva_alert: 'true'
      type: 'hardware'
    annotations:
      description: 'Target "{{ $labels.alias }}" [ cluster: "{{ $labels.cluster_name }}" / address: "{{ $labels.instance }}" ] - virtualDiskBadBlocksDetected for virtualDiskNumber {{ $labels.virtualDiskNumber }} ( {{ $labels.virtualDiskDisplayName }}).'
      runbook_url: ''
      summary: 'SNMP DELL iDRAC virtualDiskBadBlocksDetected'
